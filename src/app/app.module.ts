import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ImpExcelComponent } from './forTymetro/imp-excel/imp-excel.component';
import { ExpExcelComponent } from './forTymetro/exp-excel/exp-excel.component';

@NgModule({
  declarations: [
    AppComponent,
    ImpExcelComponent,
    ExpExcelComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
