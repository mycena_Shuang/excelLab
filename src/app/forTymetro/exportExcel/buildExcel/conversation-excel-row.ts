import { ExcelRow } from "./excelRow.abstract";

export class ConversationExcelRow extends ExcelRow{
    buildRow(): Promise<string[][]> {
        return new Promise(async resolve=>{
            resolve([...this.excelDistributionRow, ...this.excelTableKeyRow, ...this.excelTableValueRow])
        })
    }
    
}