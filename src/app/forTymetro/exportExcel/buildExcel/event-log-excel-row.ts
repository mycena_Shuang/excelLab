import { ExcelRow } from "./excelRow.abstract"

export class EventLogExcelRow extends ExcelRow{
    buildRow(): Promise<string[][]> {
        return new Promise(async resolve=>{
            resolve([...this.excelTableKeyRow, ...this.excelTableValueRow])
        })
    }
}