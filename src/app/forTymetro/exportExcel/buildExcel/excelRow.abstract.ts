import { Observable, from, reduce, map, mergeMap, tap } from "rxjs";
import { IDistribution, IKeyModel } from "../../interface.share";

export abstract class ExcelRow {
    protected excelTableKeyRow: string[][] = [];
    protected excelTableValueRow: string[][] = [];
    protected excelDistributionRow: string[][] = [];

    constructor() {}

    abstract buildRow(): Promise<string[][]>
    
    private setDistribution(distributionList: IDistribution[]): Observable<string[]> {
        return from(distributionList).pipe(
            map((distribution: IDistribution) =>distribution.chkey + "：" + distribution.value),
            reduce((distributionExcelList: string[], distributionText: string) => {
                distributionExcelList.push(distributionText)
                return distributionExcelList;
            }, [])
        )
    }

    setTableKeyRow<T>(keyList: IKeyModel[]): void {
        from(keyList).pipe(
            reduce((newKeyList: string[], keyObj: IKeyModel) => {
                newKeyList.push(keyObj.chkey);
                return newKeyList;
            }, []),
        ).subscribe(result => this.excelTableKeyRow = [result])
    }

    setTableValueRow<T>(keyList: IKeyModel[],valueList: T[]): void {
        from(valueList).pipe(
            mergeMap((valueObj: any) => {
                return from(keyList).pipe(
                    reduce((sortedValueList: string[], key: IKeyModel) => {
                        if(!(key.enkey in valueObj)) throw console.error("excel key model can't find key in value");
                        else sortedValueList.push(valueObj[key.enkey]);
                        return sortedValueList;
                    }, [])
                )
            }),
            reduce((valueNestedList: string[][], valueList:string[])=>{
                valueNestedList.push(valueList);
                return valueNestedList;
            },[]),
        ).subscribe(result => this.excelTableValueRow = result)
    }

    setDistributionRow(distribution:IDistribution[][]): void {
        from(distribution).pipe(
            mergeMap((distributionList: IDistribution[]) => this.setDistribution(distributionList)),
            reduce((distributionNestedList: string[][], distributionList: string[]) => {
                distributionNestedList.push(distributionList);
                return distributionNestedList;
            }, [])
        ).subscribe(result => this.excelDistributionRow = result)
    }
}