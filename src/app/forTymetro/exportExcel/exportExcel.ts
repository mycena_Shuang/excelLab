import { IDistribution, IKeyModel } from "../interface.share";
import * as XLSX from 'xlsx';

import { ExcelRow } from "./buildExcel/excelRow.abstract";
import { ConversationExcelRow } from "./buildExcel/conversation-excel-row";
import { ClientNotificationExcelRow } from "./buildExcel/client-notification-excel-row";
import { SystemNotificationExcelRow } from "./buildExcel/system-notification-excel-row";
import { SatisfiedExcelRow } from "./buildExcel/satisfied-excel-row";
import { ClientImageExcelRow } from "./buildExcel/client-image-excel-row";
import { EventLogExcelRow } from "./buildExcel/event-log-excel-row";
import { ProblemSetExcelRow } from "./buildExcel/problem-set-excel-row";
import { UserInfoExcelRow } from "./buildExcel/user-info-excel-row";

export class ExportExcel{
    private excelRow!: ExcelRow;

    constructor(){}

    private async makeExcel(excelRow: string[][]): Promise<void> {
        const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(excelRow);//製作工作表1
        const wb: XLSX.WorkBook = XLSX.utils.book_new(); //製作excel本體
        XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');//加入工作表
        XLSX.writeFile(wb, 'SheetJS.csv');
    }

    async buildConversationExcel<T>(distribution:IDistribution[][], keyList: IKeyModel[], valueList: T[]): Promise<void> {
        this.excelRow = new ConversationExcelRow();
        this.excelRow.setTableKeyRow(keyList);
        this.excelRow.setTableValueRow(keyList, valueList);
        this.excelRow.setDistributionRow(distribution);

        const excelRowData: string[][] = await this.excelRow.buildRow();
        this.makeExcel(excelRowData);
    }

    async buildClientNotificationExcel<T>(keyList: IKeyModel[], valueList: T[]): Promise<void> {
        this.excelRow = new ClientNotificationExcelRow();
        this.excelRow.setTableKeyRow(keyList);
        this.excelRow.setTableValueRow(keyList, valueList);

        const excelRowData: string[][] = await this.excelRow.buildRow();
        this.makeExcel(excelRowData);
    }

    async buildSystemNotificationExcel<T>(keyList: IKeyModel[], valueList: T[]): Promise<void> {
        this.excelRow = new SystemNotificationExcelRow();
        this.excelRow.setTableKeyRow(keyList);
        this.excelRow.setTableValueRow(keyList, valueList);

        const excelRowData: string[][] = await this.excelRow.buildRow();
        this.makeExcel(excelRowData);
    }

    async buildSatisfiedExcel<T>(keyList: IKeyModel[], valueList: T[]): Promise<void> {
        this.excelRow = new SatisfiedExcelRow();
        this.excelRow.setTableKeyRow(keyList);
        this.excelRow.setTableValueRow(keyList, valueList);

        const excelRowData: string[][] = await this.excelRow.buildRow();
        this.makeExcel(excelRowData);
    }

    async buildClientImageExcel<T>(keyList: IKeyModel[], valueList: T[]): Promise<void> {
        this.excelRow = new ClientImageExcelRow();
        this.excelRow.setTableKeyRow(keyList);
        this.excelRow.setTableValueRow(keyList, valueList);

        const excelRowData: string[][] = await this.excelRow.buildRow();
        this.makeExcel(excelRowData);
    }
    async buildEventLogExcel<T>(keyList: IKeyModel[], valueList: T[]): Promise<void> {
        this.excelRow = new EventLogExcelRow();
        this.excelRow.setTableKeyRow(keyList);
        this.excelRow.setTableValueRow(keyList, valueList);

        const excelRowData: string[][] = await this.excelRow.buildRow();
        this.makeExcel(excelRowData);
    }
    async buildProblemSetExcel<T>(keyList: IKeyModel[], valueList: T[]): Promise<void> {
        this.excelRow = new ProblemSetExcelRow();
        this.excelRow.setTableKeyRow(keyList);
        this.excelRow.setTableValueRow(keyList, valueList);

        const excelRowData: string[][] = await this.excelRow.buildRow();
        this.makeExcel(excelRowData);
    }
    async buildUserInfoExcel<T>(keyList: IKeyModel[], valueList: T[]): Promise<void> {
        this.excelRow = new UserInfoExcelRow();
        this.excelRow.setTableKeyRow(keyList);
        this.excelRow.setTableValueRow(keyList, valueList);

        const excelRowData: string[][] = await this.excelRow.buildRow();
        this.makeExcel(excelRowData);
    }
}