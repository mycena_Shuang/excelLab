import { Injectable } from '@angular/core';
import { ExportExcel } from './exportExcel/exportExcel';
import { ImportExcel } from './importExcel/importExcel';
import { IDistribution, IKeyModel } from './interface.share';
import { Item } from './type.share';


@Injectable({
  providedIn: 'root'
})
export class ExcelService {
  private importExcel: ImportExcel = new ImportExcel();
  private exportExcel: ExportExcel = new ExportExcel();

  constructor() { }

  downloadConversationExcel<T>(distributionModel:IDistribution[][], keyList: IKeyModel[], valueList: T[]): void {
    this.exportExcel.buildConversationExcel(distributionModel, keyList, valueList);
  }
  downloadClientNotificationExcel<T>(keyList: IKeyModel[], valueList: T[]): void {
    this.exportExcel.buildClientNotificationExcel(keyList, valueList);
  }
  downloadSystemNotificationExcel<T>(keyList: IKeyModel[], valueList: T[]): void {
    this.exportExcel.buildSystemNotificationExcel(keyList, valueList);
  }
  downloadSatisfiedExcel<T>(keyList: IKeyModel[], valueList: T[]): void {
    this.exportExcel.buildSatisfiedExcel(keyList, valueList);
  }
  downloadClientImageExcel<T>(keyList: IKeyModel[], valueList: T[]): void {
    this.exportExcel.buildClientImageExcel(keyList, valueList);
  }
  downloadEventLogExcel<T>(keyList: IKeyModel[], valueList: T[]): void {
    this.exportExcel.buildEventLogExcel(keyList, valueList);
  }
  downloadProblemSetExcel<T>(keyList: IKeyModel[], valueList: T[]): void {
    this.exportExcel.buildProblemSetExcel(keyList, valueList);
  }
  downloadUserInfoExcel<T>(keyList: IKeyModel[], valueList: T[]): void {
    this.exportExcel.buildUserInfoExcel(keyList, valueList);
  }
  setFile(file: File): Promise<Item[]> {
    return this.importExcel.getExcelContext(file);
  }
}
