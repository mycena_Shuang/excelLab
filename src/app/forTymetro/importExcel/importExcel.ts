import { ExcelData } from "./parseExcelData/excelData.abstract";
import * as XLSX from 'xlsx';
import { Item } from "../type.share";
import { KeyOnFirstRowExcel } from "./parseExcelData/keyOnFirstRowExcelData";

export class ImportExcel{
    excelData!: ExcelData;

    getExcelContext(excelFile: File):Promise<Item[]> {
        return new Promise(resolve=>{
            const reader: FileReader = new FileReader();
            
            reader.onload =async (e: any)=>{
            const bstr: string = e.target.result;
            const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});//獲得整份excel內容
            
            const wsname: string = wb.SheetNames[0];//sheetName: 分頁名稱
            const ws: XLSX.WorkSheet = wb.Sheets[wsname];//獲得工作表內容
            
            const excelDataList:string[][] =(XLSX.utils.sheet_to_json(ws, {header: 1, defval:""}));//解析工作表的內容並轉成json
            resolve(await this.parseExcelToJson(excelDataList))
        }
        reader.readAsBinaryString(excelFile)
        })
    }
    private parseExcelToJson(excelDataList: string[][]): Promise<Item[]>{
        this.excelData = new KeyOnFirstRowExcel();

        return this.excelData.parse(excelDataList);
    }
}