import { from, mergeMap, Observable, of, reduce, skip, toArray, zip } from "rxjs";
import { Item } from "../../type.share";
import { ExcelData } from "./excelData.abstract";

export class KeyOnFirstRowExcel extends ExcelData {
    constructor(){
        super();
    }

    private zipItem(keyList: string[], valueList: string[]) {
        return zip(keyList, valueList).pipe(
            reduce((item: Item, entries: any[])=>{
                const key = entries[0];
                const value = entries[1];
                if(!(key in item)) item[key] = undefined;
                item[key] = value;
                return item;
            },{}),
        )
    }

    protected makeKeyList(excelData: string[][]): string[] {
        return excelData[0];
    }

    public parse(excelData: string[][]): Promise<Item[]> {
        const excelKeyList = this.makeKeyList(excelData);
        
        return new Promise(resolve=>{
            from(excelData)
                .pipe(
                    skip(1),
                    mergeMap(excelItemValueList=>this.zipItem(excelKeyList, excelItemValueList)),
                    toArray()
                )
                .subscribe(result=>resolve(result)); 
        })
    }
}