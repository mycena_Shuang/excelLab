import { from, Observable, of } from "rxjs";
import { IDistribution, IKeyModel } from "../../interface.share";
import { Item } from "../../type.share";

export abstract class ExcelData{
    protected abstract makeKeyList(excelData: string[][]): string[]
    public abstract parse(excelData: string[][]): Promise<Item[]>
}