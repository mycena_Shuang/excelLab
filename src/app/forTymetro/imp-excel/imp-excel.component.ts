import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ExcelService } from '../excel.service';

@Component({
  selector: 'app-imp-excel',
  templateUrl: './imp-excel.component.html',
  styleUrls: ['./imp-excel.component.css']
})
export class ImpExcelComponent implements OnInit {
  //download json
  jsonURL = "";
  jsonName = "";

  constructor(
    private _sanitizer: DomSanitizer,
    private _excelService: ExcelService
  ) { }

  ngOnInit(): void {
  }

  async uploadFile(event:any){
    const inputFileList: File[] = event.target.files;
    if(inputFileList.length>0){
      console.log("import excel data transform result: ",await this._excelService.setFile(inputFileList[0]));
    };
  }

  //download json
  securityTrustResourceUrlJson() {
    return this._sanitizer.bypassSecurityTrustResourceUrl(this.jsonURL);
  }

  makeFileProperty(file: File) {
    const fileName = file.name.split(".")[0];//簡易判斷利用 點 區分名稱＋副檔名
    this.jsonName = fileName + ".json";
  }

}
