import { Component, OnInit } from '@angular/core';
import { ExcelService } from '../excel.service';
import { IDistribution, IKeyModel } from '../interface.share';

interface Employee {
  name: string,
  color: string,
  amount: number,
  expired: boolean,
  fuck: undefined
}

@Component({
  selector: 'app-exp-excel',
  templateUrl: './exp-excel.component.html',
  styleUrls: ['./exp-excel.component.css']
})

export class ExpExcelComponent implements OnInit {
  employeeList: Employee[] = [
    {
      name:"蘋果",
      color:"red",
      amount:50,
      expired: true,
      fuck: undefined
    },
    {
      name:"香蕉",
      color:"red",
      amount:50,
      expired: false,
      fuck: undefined
    }
  ];

  keySortList: IKeyModel[] = [
    {
      enkey: "name",
      chkey: "姓名"
    },
    {
      enkey: "amount",
      chkey: "數量"
    },
    {
      enkey: "color",
      chkey: "顏色"
    },
    {
      enkey: "fuck",
      chkey: "未知的欄位"
    },
    {
      enkey: "expired",
      chkey: "是否到期"
    }
  ]

  distribution: IDistribution[][] = [
    [
      {
        chkey:"聊天室",
        value:"聯天室1-女性,20,中文"
      },
      {
        chkey:"聊天室評分",
        value:"4"
      },
      {
        chkey:"聊天室處理人員",
        value:"真人課服"
      }
    ],
    [
      {
        chkey:"聊天室",
        value:"II"
      },
      {
        chkey:"聊天室處理人員",
        value:"假人"
      }
    ]
  ]
  
  constructor(
    private _excelService: ExcelService
  ) { }

  ngOnInit(): void {
  }

  testExcelService(){
    this._excelService.downloadConversationExcel<Employee>(this.distribution, this.keySortList, this.employeeList);
  }
}
