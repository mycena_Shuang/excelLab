export interface IDistribution {
    chkey: string,
    value: string
}

export interface IKeyModel{
    chkey: string,
    enkey: string
}